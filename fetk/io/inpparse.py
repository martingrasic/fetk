import os
import re
import glob
import yaml
from argparse import Namespace

import fetk.dload
import fetk.boundary
from fetk.util.lang import split
from fetk.util.schema import Schema, SchemaError, Or, Optional

seprx = "\s*,\s*"
float_regex = "[-+]?(?:(?:\d*\.\d+)|(?:\d+\.?))(?:[Ee][+-]?\d+)?"
neumann, dirichlet = 0, 1


def list_of_int(line):
    regex = "(\d+|^\d+(?:[ \t]*,[ \t]*\d+)+$)"
    if not re.search(regex, str(line)):
        raise SchemaError(f"Incorrect element specification at {line}") from None
    return True


def list_of_float(line):
    regex = f"^{float_regex}+(?:[ \t]*,[ \t]*{float_regex}+)+$"
    if not re.search(regex, str(line)):
        raise SchemaError(f"Incorrect element specification at {line}") from None
    return True


def parse_file(file):
    """Read a FEM input file"""
    p = parser()
    p.parse(file)
    return p


class parser:
    def __init__(self):
        self.dload = []
        self.boundary = []

    def append_dload(self, type, elem_no, side_no, *components):
        values = list(components) + [0.0] * (3 - len(components))
        load = [type, elem_no, side_no, values]
        self.dload.append(load)

    def append_boundary(self, type, node_no, dof_label, magnitude):
        self.boundary.append([type, node_no, dof_label, magnitude])

    def parse(self, file):
        self.dload = []
        self.boundary = []
        ns = self.load_and_validate(file)
        inp = ns.data["input"]
        self.jobid = self.load_jobid(inp.get("jobid"))
        self.nodes = self.load_node(inp["node"])
        self.elements = self.load_element(inp["element"])
        self.materials = self.load_materials(inp["materials"])
        self.element_blocks = self.load_element_blocks(inp["element blocks"])
        self.load_boundary(inp["boundary"])
        self.load_cload(inp.get("cload"))
        self.load_dload(inp.get("dload"))
        self.load_dflux(inp.get("dflux"))
        self.load_film(inp.get("film"))

    @property
    def input_schema(self):
        return Schema(
            {
                "input": {
                    Optional("jobid"): str,
                    "node": [self.node_defn],
                    "element": [list_of_int],
                    "element blocks": [
                        {
                            "block": {
                                Optional("name"): Or(int, str),
                                "element type": str,
                                Optional("element properties"): {str: object},
                                "material": Or(int, str),
                                "elements": list_of_int,
                            }
                        }
                    ],
                    "materials": [
                        {
                            "material": {
                                "id": Or(int, str),
                                "model": str,
                                "parameters": {str: object},
                            }
                        }
                    ],
                    Optional("element sets"): [
                        {"set": {"name": str, "elements": list_of_int}}
                    ],
                    Optional("node sets"): [
                        {"set": {"name": str, "nodes": list_of_int}}
                    ],
                    Optional("boundary"): [self.boundary_defn],
                    Optional("cload"): [self.cload_defn],
                    Optional("dload"): [self.dload_defn],
                    Optional("dflux"): [self.dflux_defn],
                    Optional("film"): [self.film_defn],
                }
            }
        )

    def load_and_validate(self, file):
        ns = Namespace(filename=None, data=None)
        if hasattr(file, "read"):
            ns.data = yaml.safe_load(file)
            ns.filename = getattr(file, "name", "string")
        elif not os.path.exists(file):
            ns.data = yaml.safe_load(file)
        else:
            ns.filename = file
            with open(file) as fh:
                ns.data = yaml.safe_load(fh)
        self.validate(ns.data)
        return ns

    def load_jobid(self, jobid_spec):
        if jobid_spec is None:
            jobs = set(
                [
                    os.path.splitext(os.path.basename(f))[0]
                    for f in glob.glob("./Job-*.*")
                ]
            )
            for i in range(1, 1001):
                if f"Job-{i}" not in jobs:
                    return f"Job-{i}"
        return jobid_spec

    def load_node(self, node_spec):
        node = []
        for (i, row) in enumerate(node_spec):
            xn, *xc = split(row, sep=",")
            if len(xc) > 3:
                raise ValueError(f"Node {xn} has more than 3 dimensions")
            node.append([int(xn)] + [float(_) for _ in xc])
        return node

    def load_element(self, element_spec):
        element = []
        for row in element_spec:
            xel, *nodes = split(row, sep=",", transform=int)
            element.append([xel] + list(nodes))
        return element

    def load_materials(self, material_spec):
        materials = []
        for (i, field) in enumerate(material_spec, start=1):
            field = field["material"]
            material = {
                "model": field["model"],
                "id": field["id"],
                "name": field.get("name", f"Material-{i}"),
                "parameters": field["parameters"],
            }
            materials.append(material)
        return materials

    def load_element_blocks(self, block_spec):
        blocks = []
        for (id, field) in enumerate(block_spec, start=1):
            field = field["block"]
            block = {
                "name": field.get("name", f"Block-{id}"),
                "material": field["material"],
                "element type": field["element type"],
                "element properties": field.get("element properties", {}),
                "elements": split(str(field["elements"]), sep=",", transform=int),
            }
            blocks.append(block)
        return blocks

    def load_boundary(self, boundary_spec):
        """NODE ID, DOF, MAGNITUDE"""
        for spec in boundary_spec:
            spec = split(spec, sep=",")
            xn = int(spec[0])
            dof_label = spec[1].lower()
            magnitude = float(spec[2])
            self.append_boundary(fetk.boundary.dirichlet, xn, dof_label, magnitude)
        return

    def load_cload(self, cload_spec):
        """NODE ID, DOF, MAGNITUDE"""
        if not cload_spec:
            return
        for spec in cload_spec:
            spec = split(spec, sep=",")
            xn = int(spec[0])
            dof_label = spec[1].lower()
            magnitude = float(spec[2])
            self.append_boundary(fetk.boundary.neumann, xn, dof_label, magnitude)
        return

    def load_dload(self, dload_spec):
        """ELEMENT ID, LABEL[FACE], COMPONENTS"""
        if not dload_spec:
            return
        for spec in dload_spec:
            side_no = -1
            spec = split(spec, sep=",")
            elem_no = int(spec[0])
            label = spec[1].lower()
            components = [float(_) for _ in spec[2:]]
            if label == "p":
                if len(components) != 1:
                    raise ValueError(
                        f"Expected a single load component in dload {spec}"
                    )
                type = fetk.dload.pressure
            elif label == "bf":
                type = fetk.dload.body_force
            elif label in ("bx", "by", "bz"):
                if len(components) != 1:
                    raise ValueError(
                        f"Expected a single load component in dload {spec}"
                    )
                dof = {"x": 0, "y": 1, "z": 2}[label[1]]
                magnitude = components[0]
                components = [0.0] * 3
                components[dof] = magnitude
                type = fetk.dload.body_force
            elif label in ("px", "py", "pz"):
                if len(components) != 1:
                    raise ValueError(
                        f"Expected a single load component in dload {spec}"
                    )
                dof = {"x": 0, "y": 1, "z": 2}[label[1]]
                magnitude = components[0]
                components = [0.0] * 3
                components[dof] = magnitude
                type = fetk.dload.surface_force
            elif re.search("f\d+", label):
                side_no = int(label[1:])
                type = fetk.dload.surface_force
            else:
                raise ValueError(f"Unknown dload type {label}")
            self.append_dload(type, elem_no, side_no, *components)
        return

    def load_dflux(self, dflux_spec):
        """ELEMENT ID, LABEL[FACE], COMPONENTS"""
        if not dflux_spec:
            return
        for spec in dflux_spec:
            side_no = -1
            spec = split(spec, sep=",")
            elem_no = int(spec[0])
            label = spec[1].lower()
            components = [float(_) for _ in spec[2:]]
            if label == "bf":
                if len(components) != 1:
                    raise ValueError(
                        f"Expected a single load component in dflux {spec}"
                    )
                type = fetk.dload.body_flux
            elif re.search("f\d+", label):
                side_no = int(label[1:])
                type = fetk.dload.surface_flux
            else:
                raise ValueError(f"Unknown dflux type {label}")
            self.append_dload(type, elem_no, side_no, *components)
        return

    def load_film(self, film_spec):
        """ELEMENT ID, LABEL[FACE], COMPONENTS"""
        if not film_spec:
            return
        for spec in film_spec:
            side_no = -1
            spec = split(spec, sep=",")
            elem_no = int(spec[0])
            label = spec[1].lower()
            components = [float(_) for _ in spec[2:]]
            if re.search("f\d+", label):
                if len(components) != 2:
                    raise ValueError(f"Expected a 2 components in film {spec}")
                side_no = int(label[1:])
                type = fetk.dload.surface_film
            else:
                raise ValueError(f"Unknown film type {label}")
            self.append_dload(type, elem_no, side_no, *components)
        return

    @staticmethod
    def node_defn(line):
        regex = f"\d+{seprx}{float_regex}(?:{seprx}{float_regex})+$"
        if not re.search(regex, line.strip()):
            raise SchemaError(f"Incorrect node specification at {line}") from None
        return True

    @staticmethod
    def boundary_defn(line):
        """NODE ID, LABEL, MAGNITUDE"""
        labels = "(x|y|z|t|xyz|xy|xz|yz)"
        regex = f"\d{seprx}{labels}{seprx}{float_regex}"
        if not re.search(regex, line.lower()):
            raise SchemaError(f"Incorrect boundary specification at {line}") from None
        return True

    @staticmethod
    def cload_defn(line):
        """NODE ID, LABEL, MAGNITUDE"""
        labels = "(x|y|z|xyz|xy|xz|yz)"
        regex = f"\d{seprx}{labels}{seprx}{float_regex}"
        if not re.search(regex, line.strip().lower()):
            raise SchemaError(f"Incorrect cload specification at {line}") from None
        return True

    @staticmethod
    def dload_defn(line):
        """ELEMENT ID, BF, DX[, DY[, DZ]]
           ELEMENT ID, P, MAGNITUDE
           ELEMENT ID, Fn, DX[, DY[, DZ]]
           ELEMENT ID, B[XYZ], MAGNITUDE
           ELEMENT ID, P[XYZ], MAGNITUDE
        """
        string = line.strip().lower()
        regex = f"\d+{seprx}(bf|p|bx|by|bz|px|py|pz|f\d+)(?:{seprx}{float_regex})+$"
        match = re.search(regex, string)
        if not match:
            raise SchemaError(f"Incorrect dload specification at {line}") from None
        return True

    @staticmethod
    def dflux_defn(line):
        """ELEMENT ID, BF, MAGNITUDE
           ELEMENT ID, Fn, MAGNITUDE[, X[, Y[, Z]]]
        """
        string = line.strip().lower()
        regex = f"\d+{seprx}(bf|f\d+)(?:{seprx}{float_regex})+$"
        match = re.search(regex, string)
        if not match:
            raise SchemaError(f"Incorrect dflux specification at {line}") from None
        return True

    @staticmethod
    def film_defn(line):
        """ELEMENT ID, Fn, MAGNITUDE[, X[, Y[, Z]]]
        """
        string = line.strip().lower()
        regex = f"\d+{seprx}f\d+(?:{seprx}{float_regex}){{2}}$"
        match = re.search(regex, string)
        if not match:
            raise SchemaError(f"Incorrect film specification at {line}") from None
        return True

    def validate(self, data):
        self.input_schema.validate(data)
